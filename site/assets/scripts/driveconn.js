// driveconn.js
'use strict';

const client_id = '522981563938-3maj1le20bgq23cgn7l5nrf41et43kom.apps.googleusercontent.com';
const scopes = 'https://www.googleapis.com/auth/drive.appdata'
var auth = null;

var recvAuthMsg = (dat) => {
	var mat = dat.match(/access_token=([^&]+)&/);
	if (mat !== null) {
		var signedIn = (auth !== null);
		window.auth = {};
		auth.token = mat[1];
		getUserInfo().then((r) => {
			if (!signedIn) {
				auth.user = r.user.emailAddress;
				localStorage.setItem('userSub', auth.user);
				showSignIn(r.user.displayName);
			}
		});
		auth.timeout = setTimeout(renewAuth, 45*60*1000);
	} else {
		signOut();
	}
};

var requestNewAuth = function () {
	window.open(
		'https://accounts.google.com/o/oauth2/v2/auth?scope='+scopes
		+'&response_type=token&client_id='+client_id
		+'&redirect_uri=http://localhost:3010/auth',
		'AuthWin', 'height=600,width=600,top=100,left=100'
	);
};

var renewAuth = function (u) {
	if (!u) {
		u = auth.user;
	}
	authFrame.contentWindow.location =
		'https://accounts.google.com/o/oauth2/v2/auth?scope='+scopes
		+'&response_type=token&client_id='+client_id
		+'&redirect_uri=http://localhost:3010/auth'
		+'&prompt=none&login_hint='+encodeURIComponent(u);
};

var getUserInfo = function () {
	return doGet('https://www.googleapis.com/drive/v3/about?fields=user');
};

var signOut = function () {
	if (auth !== null) {
		clearTimeout(auth.timeout);
		window.auth = null;
	}
	localStorage.removeItem('userSub');
	signButton.innerHTML = 'Sign In';
	userNameSpan.innerHTML = '';
	fileHolder.innerHTML = '';
	signButton.onclick = requestNewAuth;
};

var showSignIn = function (name) {
	userNameSpan.innerHTML = name;
	signButton.innerHTML = 'Sign Out';
	signButton.onclick = signOut;
	getFileList().then((fl) => {
		window.filelist = fl;
		fileHolder.innerHTML = '';
		for (var i = 0; i < filelist.length; i++) {
			var o = document.createElement('p');
			o.innerHTML = filelist[i].name;
			fileHolder.appendChild(o);
		}
	});
};

var statusOkay = function (status) {
	return (status >= 200) && (status < 300);
}

var getFileList = function () {
	return new Promise((resolve, reject) => {
		var fl = [];
		var helper = (pt) => {
			var u = 'https://www.googleapis.com/drive/v3/files?spaces=appDataFolder&pageToken=' + pt;
			doGet(u).then((resp) => {
				Array.prototype.push.apply(fl, resp.files);
				if ('nextPageToken' in resp) {
					helper(resp.nextPageToken);
				} else {
					resolve(fl);
				}
			}).catch(reject);
		};
		helper('');
	});
}

var makeXHR = function (verb, url) {
	var xhr = new XMLHttpRequest();
	xhr.open(verb, url);
	xhr.setRequestHeader('Authorization', 'Bearer ' + auth.token);
	return xhr;
};

var doGet = function (url) {
	return new Promise((resolve, reject) => {
		var xhr = makeXHR('GET', url);
		xhr.responseType = 'json'
		xhr.onloadend = () => {
			if (statusOkay(xhr.status)) {
				resolve(xhr.response);
			} else {
				reject(xhr.status);
			}
		};
		xhr.send();
	});
};

var createFile = function (name) {
	return new Promise((resolve, reject) => {
		var xhr = makeXHR('POST', 'https://www.googleapis.com/drive/v3/files');
		xhr.setRequestHeader('Content-Type', 'application/json');
		xhr.responseType = 'json';
		xhr.onloadend = () => {
			if (statusOkay(xhr.status)) {
				resolve(xhr.response.id);
			} else {
				reject(xhr.status);
			}
		};
		xhr.send(JSON.stringify({name: name, parents: ['appDataFolder']}));
	});
};

var getControlFile = function (fid) {
	return new Promise((resolve, reject) => {
		var xhr = makeXHR('GET', 'https://www.googleapis.com/drive/v3/files/'+fid+'?alt=media');
		xhr.responseType = 'json';
		xhr.onloadend = () => {
			if (statusOkay(xhr.status)) {
				resolve(xhr.response);
			} else {
				reject(xhr.status);
			}
		};
		xhr.send();
	});
};

var getBufFile = function (fid) {
	return new Promise((resolve, reject) => {
		var xhr = makeXHR('GET', 'https://www.googleapis.com/drive/v3/files/'+fid+'?alt=media');
		xhr.responseType = 'arraybuffer';
		xhr.onloadend = () => {
			if (statusOkay(xhr.status)) {
				resolve(xhr.response);
			} else {
				reject(xhr.status);
			}
		};
		xhr.send();
	});
};

var setControlFile = function (fid, body) {
	return setFile(fid, JSON.stringify(body));
};

var setFile = function (fid, body) {
	return new Promise((resolve, reject) => {
		var xhr = makeXHR('PATCH', 'https://www.googleapis.com/upload/drive/v3/files/'+fid+'?uploadType=media');
		xhr.responseType = 'json';
		xhr.onloadend = () => {
			if (statusOkay(xhr.status)) {
				resolve();
			} else {
				reject(xhr.status);
			}
		};
		xhr.send(body);
	});
};

var removeFile = function (fid) {
	return new Promise((resolve, reject) => {
		var xhr = makeXHR('DELETE', 'https://www.googleapis.com/drive/v3/files/'+fid);
		xhr.responseType = 'json';
		xhr.onloadend = () => {
			if (statusOkay(xhr.status)) {
				resolve();
			} else {
				reject(xhr.status);
			}
		};
		xhr.send();
	});
};

var signButton = document.getElementById('signButton');
var userNameSpan = document.getElementById('userName');
var fileHolder = document.getElementById('files');
var authFrame = document.getElementById('authFrame');

(function () {
	var u = localStorage.getItem('userSub');
	if (u !== null) {
		renewAuth(u);
	} else {
		signOut();
	}
})();
