// main.js
'use strict';

var extractFileData = function (file) {
	return new Promise((resolve, reject) => {
		var fr = new FileReader();
		fr.onloadend = () => {
			if (fr.result === null) {
				reject();
			} else {
				resolve({name: file.name, data: fr.result});
			}
		};
		fr.readAsArrayBuffer(file);
	});
};

var fileUp = document.getElementById('fileUp');
fileUp.onchange = () => {
	console.log(fileUp.files);
};

//navigator.serviceWorker.register('cacher.js');
