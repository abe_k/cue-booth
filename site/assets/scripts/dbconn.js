// dbconn.js
'use strict';

const db_version = 1;

var openDb = function () {
	return new Promise((resolve, reject) => {
		var req = indexedDB.open('db', db_version);
		req.onupgradeneeded = (e) => {
			var dbup = req.result;
			if (e.oldVersion < 1) {
				dbup.createObjectStore('info');
				dbup.createObjectStore('show', {autoIncrement: true});
				dbup.createObjectStore('media', {autoIncrement: true});
			}
		};
		req.onsuccess = () => {
			window.db = req.result;
			resolve();
		};
		req.onerror = () => {
			reject(req.error);
		};
	});
};

var getInDb = function (store, key) {
	return new Promise((resolve, reject) => {
		var req = db.transaction(store).objectStore(store).get(key);
		req.onsuccess = () => {
			if (req.result !== undefined) {
				resolve(req.result);
			} else {
				reject();
			}
		};
		req.onerror = () => {
			reject(req.error);
		};
	});
};

var setInDb = function (store, key, value) {
	return new Promise((resolve, reject) => {
		var tros = db.transaction(store, 'readwrite').objectStore(store);
		var req;
		if (key === null) {
			req = tros.put(value);
		} else {
			req = tros.put(value, key);
		}
		req.onsuccess = () => {
			resolve(req.result);
		};
		req.onerror = () => {
			reject(req.error);
		};
	});
};

var removeInDb = function (store, key) {
	return new Promise((resolve, reject) => {
		var req = db.transaction(store, 'readwrite').objectStore(store).delete(key);
		req.onsuccess = () => {
			resolve();
		};
		req.onerror = () => {
			reject(req.error);
		};
	});
};

var listInDb = function (store) {
	return new Promise((resolve, reject) => {
		var list = [];
		var req = db.transaction(store).objectStore(store).getAllKeys();
		req.onsuccess = () => {
			resolve(req.result);
		};
		req.onerror = () => {
			reject(req.error);
		};
	});
};
