// audplay.js
'use strict';

var audctx = new AudioContext();

var SoundClip = class {
	constructor (buf) {
		this.buf = buf;
		this.gn = audctx.createGain();
		this.gain = this.gn.gain;
		this.gn.connect(audctx.destination);
	}
	playFixed (when, from, to, fin, fout, vol) {
		this.src = audctx.createBufferSource();
		this.src.buffer = this.buf;
		this.src.connect(this.gn);
		this.gain.cancelScheduledValues(when);
		if (fin > 0) {
			this.gain.value = 0;
			this.gain.setValueAtTime(0, when);
			this.gain.linearRampToValueAtTime(vol, when+fin);
		} else {
			this.gain.value = vol;
		}
		var dur = to-from;
		if (fout > 0) {
			this.gain.setValueAtTime(vol, when+dur-fout);
			this.gain.linearRampToValueAtTime(0, when+dur);
		}
		this.src.start(when, from, dur);
	}
};
