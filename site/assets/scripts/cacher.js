// cacher.js

var c_version = 1;

var c_name = 'readthrough-v' + c_version;

self.addEventListener('activate', function(e) {
	e.waitUntil(caches.keys().then(function(names) {
		return Promise.all(names.map(function(name) {
			if (name !== c_name) {
				return caches.delete(name);
			}
		}));
	}));
});

self.addEventListener('fetch', function(e) {
	console.log(e.request.url);
	e.respondWith(caches.open(c_name).then(function(cache) {
		return cache.match(e.request).then(function(response) {
			if (response) {
				return response;
			}
			return fetch(e.request.clone()).then(function(response) {
				if (response.type == 'basic' && response.status < 400) {
					cache.put(e.request, response.clone());
				}
				return response;
			});
		});
	}));
});
